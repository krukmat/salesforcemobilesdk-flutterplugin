// Create a MockClient using the Mock class provided by the Mockito package.
// Create new instances of this class in each test.
import 'package:flutter_test/flutter_test.dart';
import 'package:sfplugin/request_data.dart';
import 'package:http/http.dart' show Response;
import 'package:http/testing.dart';


main() {
  group('fetchPost', () {
    test('returns a Post if the http call completes successfully', () async {
      var client = new MockClient((request) async{
          return Response('{"title": "Test"}', 200);
        });
      expect(await fetchPost(client), isInstanceOf<Post>());
    });

    test('throws an exception if the http call completes with an error', () {
      final client = new MockClient((request) async{
          return Response('Not Found', 404);
        });
      expect(fetchPost(client), throwsException);
    });
  });
}